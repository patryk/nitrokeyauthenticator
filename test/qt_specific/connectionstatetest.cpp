/*
 * BSD 2-Clause License
 *
 * Copyright (c) 2020, Agnieszka Cicha-Cisek
 * Copyright (c) 2020, Patryk Cisek
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/


#include <QSignalSpy>
#include <QtTest/QtTest>

#include "connectionstate.h"

class ConnectionStateTests : public QObject
{
    Q_OBJECT
    static constexpr int SHORT_TIMEOUT_MSEC = 100;

private slots:
    void stateNeverChangesToConnectedIfNotEverConnected() {
        // Given
        ConnectionState state;
        QSignalSpy spy(&state, &ConnectionState::connectionStateChanged);
        state.setCurrentState(false);
        state.setCurrentState(false);
        state.setCurrentState(false);

        // When
        spy.wait(SHORT_TIMEOUT_MSEC);

        // Then
        QTRY_COMPARE(spy.count(), 0);
        }

    void stateChangesToConnectedWhenConnected() {
        // Given
        ConnectionState state;
        QSignalSpy spy(&state, &ConnectionState::connectionStateChanged);
        state.setCurrentState(false);
        state.setCurrentState(false);
        state.setCurrentState(true);
        state.setCurrentState(true);
        state.setCurrentState(true);

        // When
        spy.wait(SHORT_TIMEOUT_MSEC);

        // Then
        QTRY_COMPARE(spy.count(), 1);
        auto signalArgs = spy.takeFirst();
        QTRY_COMPARE(signalArgs.at(0).toBool(), true);
    }

    void stateChangesAsManyTimesAsExpected() {
        // Given
        ConnectionState state;
        QSignalSpy spy(&state, &ConnectionState::connectionStateChanged);
        state.setCurrentState(false);
        state.setCurrentState(false);
        // 1st change (to true)
        state.setCurrentState(true);
        state.setCurrentState(true);
        state.setCurrentState(true);
        // 2nd change (to false)
        state.setCurrentState(false);
        // 3rd change (to true)
        state.setCurrentState(true);
        state.setCurrentState(true);
        state.setCurrentState(true);
        // 4th change (to false)
        state.setCurrentState(false);
        state.setCurrentState(false);

        // When
        spy.wait(SHORT_TIMEOUT_MSEC);

        // Then
        QTRY_COMPARE(spy.count(), 4);
        auto signalArgs1 = spy.takeFirst();
        auto signalArgs2 = spy.takeFirst();
        auto signalArgs3 = spy.takeFirst();
        auto signalArgs4 = spy.takeFirst();
        QTRY_COMPARE(signalArgs1.at(0).toBool(), true);
        QTRY_COMPARE(signalArgs2.at(0).toBool(), false);
        QTRY_COMPARE(signalArgs3.at(0).toBool(), true);
        QTRY_COMPARE(signalArgs4.at(0).toBool(), false);
    }

};

QTEST_MAIN(ConnectionStateTests)
#include "connectionstatetest.moc"
