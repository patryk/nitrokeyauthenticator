/*
 * BSD 2-Clause License
 *
 * Copyright (c) 2020, Agnieszka Cicha-Cisek
 * Copyright (c) 2020, Patryk Cisek
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef SCREENSHOTTER_H
#define SCREENSHOTTER_H

#include <QErrorMessage>
#include <QTimer>

#include <zbar/QZBar.h>

/**
 * @brief Encapsulates logic of taking screenshot and processing Qr-Code embedded in it.
 *
 * This class encapsulates the logic of taking screenshot using 2 methods. 1st it tries
 * to use GNOME Shell's method. If it won't work, it falls back to taking screenshot in
 * X11. Supporting the 2 methods is important, because GNOME by default uses Wayland
 * (GDM's default). If this is the case, standard method of taking screenshot, that Qt's
 * using will not work. The obtained image will be a black screen. It is also important
 * to note, that Wayland does not expose API for reading content of the screen. That is
 * why Qt's not supporting that. Each and every Wayland WM has to keep track of that on
 * its own. That's why we have to use GNOME Shell here.
 *
 * In the future this class might need revisiting. For now, supporting GNOME-only Wayland
 * use-case seems to be enough. But when Wayland gains more popularity, this might not be
 * enough. Also, maybe Wayland will support reading screen by that time.
 *
 * One last, but very important remark: GNOME Shell uses a temp file to store the requested
 * screenshot. NitrokeyAuthenticator deletes this temporary file right after processing it,
 * but this is additional threat surface -- the screenshot by definition contains sensitive
 * data: the Qr-Code contains encoded pre-shared secret used to generate TOTP codes.
 */
class Screenshotter : public QObject
{
    Q_OBJECT

    QTimer qrCodeDetectionErrorTimer;
    zbar::QZBar scanner;
    const QWindow *windowHandle;
    QScreen *screen;
    QSize screenshotSize;
    QErrorMessage errorMessage;

public:
    /**
     * @brief Constructs instance of Screenshotter.
     *
     * @param windowHandle Window handle is needed by Qt to take screenshot using X11
     * interface.
     * @param parent Standard Qt's QObject parent.
     */
    explicit Screenshotter(const QWindow *windowHandle, QObject *parent = nullptr);

    /**
     * @brief Takes screenshot and asynchronously processes Qr-Code embedded in that
     * screenshot.
     *
     * This method takes screenshot and then uses ZBar to process it. ZBar will try
     * to detect Qr-Code and decode it. If that succeeds, a signal:
     * qrCodeDecoded() is emitted. If detection of the Qr-Code fails,
     * qrCodeDetectionError() is emitted.
     */
    void takeScreenshot();

signals:
    /**
     * @brief Emitted when Qr-Code is successfully detected and decoded in
     * the screenshot.
     *
     * @param decodedText Decoded text, that was encoded in the Qr-Code.
     */
    void qrCodeDecoded(const QString &decodedText);

    /**
     * @brief Emitted when detecting Qr-Code in the screenshot failed.
     */
    void qrCodeDetectionError();

private slots:
    QImage takeX11Screenshot();
    QImage takeWaylandScreenshot();
    void onQrCodeDecoded(const QString &decodedText);
};

#endif // SCREENSHOTTER_H
